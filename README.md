# Moss stem axis
## Projection of signal intensity on a 1D axis of a stem 

----

### Authors
Guillaume Cerutti, Yoan Coudert (Laboratoire RDP, ENS Lyon)

### Installation

#### Pre-requisite: install `conda`

If `conda` is not already installed (open a terminal window and type `conda` to check), you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.

#### Download the notebook

Open a new terminal window and navigate to the folder where you want to download the notebook. For instance to place it on your Desktop:

```
cd ~/Desktop
```

You can also type `cd `, and then simply drag and drop the folder from your File System Browser / Finder into the terminal window. 

Download and enter the repository containing the notebook:

```bash
git clone https://gitlab.inria.fr/gcerutti/moss_stem_axis.git
cd moss_stem_axis
```

#### Create environment with dependencies

Create a new [Virtual Environment](https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/environments.html) containing all the dependencies required to run the notebook, using the `evironment.yml` file:

```bash
conda env create -f environment.yml
```

> It may take several minutes to solve for compatible versions of the dependencies.

#### Update a previous install

To update your install, it is necessary to remove the changes you may have made on the notebook before retrieving the new version:


> WARNING: all your changes will be lost!

```bash
git stash && git stash drop && git pull
```

> Make sure to navigate to the `moss_stem_axis` directory before updating

### Use

Everytime you want to use the notebook, you'll have to activate the environment before launching Jupyter:

```bash
conda activate stem_axis
jupyter notebook
```

> Make sure to navigate to the `moss_stem_axis` directory before lauching Jupyter
